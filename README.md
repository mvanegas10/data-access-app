# Jupyter Notebook DEVS
This repository contains the different jupyter notebook developments during 2020-2021.
It contains a jupyter notebook template at `notebook/00_notebook_template.ipynb`. This template presents basic rutines as:
- Connecting to Redshift for a given user defined at `config/default.ini`

## Getting started

### Prerequisites

* python (>= 3.7 required)
* pip (>= 20.0.2 required)

### Installation
In order to deploy the application, you need to install the prerequisites listed in the requirements.txt file. Do the following:

1. Clone the repo
```sh
git clone https://gitlab.com/bigdata-media/jupyter-notebook-devs.git
```
2. Install PIP packages
```sh
pip install -r requirements.txt
```

### Configuration
Please create a configuration file at `config/default.ini` with the following information:
```ini
[DATABASE] # Redshift database
host = <host>
port = <port>
database = <database>
username = <username>
password = <password>

[DB_PROD] # Reco PostgreSQL database production
host = <host>
port = <port>
database = <database>
username = <username>
password = <password>

[DB_DEV] # Reco PostgreSQL database development 
host = <host>
port = <port>
database = <database>
username = <username>
password = <password>

[DEFAULT]
AWS_ACCESS_KEY_ID = <AWS_ACCESS_KEY_ID>
AWS_DEFAULT_REGION = <AWS_DEFAULT_REGION>
AWS_SECRET_ACCESS_KEY = <AWS_SECRET_ACCESS_KEY>

```


### Serving the notebooks
After installing the requirements, you can serve the notebooks using the command:
```sh
jupyter notebook
```